This script was developed for teachers at BU MET who wanted to see how long each student in their class spent logged in to a lab computer. The script parses a CSV file containing information about each session in all of the lab computers and outputs a new sorted CSV with each student's total logged on time. The results are then sent by email.

usage: ./met_lab_report.py [.csv file]