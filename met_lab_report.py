#!/usr/bin/env python

import sys
import datetime
import smtplib

from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart

from datetime import date

csv_file  = open(sys.argv[1], "r")
csv_list  = []
clean_csv = []

blank_row = ",,,,\r\n"

to_datetime = lambda time: datetime.datetime.strptime(time,"%H:%M:%S.%f")

# takes a csv for a lab and returns a dictionary user -> time
def get_times(lab):
	user_time = {}
	for i in range(len(lab)):
		this_row = lab[i][:]
		s = this_row.pop(4)
		t = to_datetime(this_row.pop(3))
		if s == "logon":
			for j in range(i+1,len(lab)):
				next_row = lab[j][:]
				next_s = next_row.pop(4)
				next_t = to_datetime(next_row.pop(3))
				if this_row == next_row and s != next_s:
					user = lab[j][0]
					time_spent = next_t - t
					if user not in user_time:
						user_time[user] = time_spent
					else:
						user_time[user] += time_spent
					break
	return user_time

# format fields for sorting and place in list
for line in csv_file:
  if line == blank_row: continue
  row = line.split(",")

  row[1] = row[1].strip(" ")
  # remove day prefix
  row[2] = row[2].split()[-1]
  row[3] = row[3].strip(" ")
  if row[3].isdigit(): continue # time not in proper format. must fix logger
  # remove newlines
  row[4] = row[4].replace(" \r\n","").strip(" ")

  csv_list.append(row)

start_date = date.today() - datetime.timedelta(days=7)
start_date = start_date.strftime("%D")
  
# get list of dates so we can filter() them
dates = []
for row in clean_csv:
	if row[2] not in dates and row[2] >= start_date:
		dates.append(row[2])
  
# sort by date
csv_list = sorted(csv_list, key=lambda row: row[2])

# remove "duplicate" logins/logoffs (double login and logoffs within milliseconds?)
for i in range(len(csv_list)-1):
  this_row = csv_list[i][:]
  next_row = csv_list[i+1][:]
  this_time = to_datetime(this_row.pop(3))
  next_time = to_datetime(next_row.pop(3))
  if not(this_row == next_row and (next_time.microsecond-this_time.microsecond<1000000)): # all of the duplicate logins/logoffs happen within microseconds of eachother
    clean_csv.append(csv_list[i])
clean_csv.append(csv_list[i+1])

# make final csv
final_csv = []
labs = []
for date in dates:
	todays_csv = filter(lambda x: date in x, clean_csv)
	for row in todays_csv:
		if row[1] not in labs:
			labs.append(row[1])
	for lab in labs:
		lab_csv = filter(lambda x: lab in x, todays_csv)
		ut = get_times(lab_csv)
		for user in ut:
		  final_csv.append(','.join([date,lab,user,str(ut[user]).split('.')[0]]))
	labs = []

print final_csv
	
"""
# prepare email
server   = 'smtp.bu.edu'
sender   = 'metit@bu.edu'
receiver = 'burstein@bu.edu'
body = 'Attached is the CSV file.'

msg = MIMEMultipart()
msg.attach(MIMEText(body, 'plain'))

msg['From']    = sender
msg['To']      = receiver
msg['Subject'] = 'Weekly Automated VLAB Usage Report'


attachment = MIMEText('\n'.join(final_csv))
attachment.add_header('Content-Disposition', 'attachment', filename='asdf.csv')
msg.attach(attachment)

smtp = smtplib.SMTP_SSL(server, 465)
smtp.sendmail(msg['From'], msg['To'], msg.as_string())

smtp.close()
"""

csv_file.close()
